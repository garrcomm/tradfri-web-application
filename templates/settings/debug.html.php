<?php

/* @var $baseHref string */
/* @var $tradfriGateway \Garrcomm\Tradfri\Model\TradfriGateway */
/* @var $tradfriGroups \Garrcomm\Tradfri\Model\TradfriGroup[] */
/* @var $tradfriDevices \Garrcomm\Tradfri\Model\BaseTradfriDevice[] */
/* @var $tradfriScenes \Garrcomm\Tradfri\Model\TradfriScene[] */

$output =
    'var tradfriGateway = ' . json_encode($tradfriGateway, JSON_PRETTY_PRINT) . ';' . PHP_EOL
    . PHP_EOL
    . 'var tradfriScenes = ' . json_encode($tradfriScenes, JSON_PRETTY_PRINT) . ';' . PHP_EOL
    . PHP_EOL
    . 'var tradfriDevices = ' . json_encode($tradfriDevices, JSON_PRETTY_PRINT) . ';' . PHP_EOL
    . PHP_EOL
    . 'var tradfriGroups = ' . json_encode($tradfriGroups, JSON_PRETTY_PRINT) . ';' . PHP_EOL
;
?>
<div class="row">
    <div class="col-12">
        <form>
            <textarea class="form-control" style="overflow: hidden" rows="<?= substr_count($output, "\n") ?>"><?= htmlspecialchars($output) ?></textarea>
        </form>
    </div>
</div>
