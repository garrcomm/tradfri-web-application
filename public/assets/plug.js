// Toggle buttons for plugs clickable
$('[data-device-type=plug] [data-device-toggle]').click(function(e) {
    // Execute call
    $.ajax({url: $(this).attr('href'), data: { ajax: 1 }, success: function(data) {
        refreshPlug(data);
        updateGroupHtml($('[data-device-id=' + data.id + ']').data('group-id'));
    }});

    // Cancel click event
    e.preventDefault();
    return false;
});

function refreshPlug(deviceData) {
    let deviceElement = $('[data-device-type=plug][data-device-id=' + deviceData.id + ']');
    // Change toggle visibility
    if (deviceData.on) {
        deviceElement.find('.show-when-on').removeClass('d-none');
        deviceElement.find('.show-when-off').addClass('d-none');
    } else {
        deviceElement.find('.show-when-on').addClass('d-none');
        deviceElement.find('.show-when-off').removeClass('d-none');
    }
}
