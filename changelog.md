# Changelog

Here you'll find the changes between versions of this application.

## [Unreleased]

## [0.1.2-alpha] - 2021-05-02

### Added

- Added icons for scenes ([issue #2](https://bitbucket.org/garrcomm/fonsterstod-windows-client/issues/2/use-scene-icons))
- Made it possible to toggle full groups at once ([issue #3](https://bitbucket.org/garrcomm/fonsterstod-windows-client/issues/3/make-it-possible-to-control-a-full-group))

### Fixed

- Performance improvement (less requests for updating devices)
- Scene overview page has improved buttons

## [0.1.1-alpha] - 2021-04-29

### Added

- Added support for IKEA Blinds (untested though since I don't have the correct hardware)
- Added loading screens ([issue #4](https://bitbucket.org/garrcomm/fonsterstod-windows-client/issues/4/add-loading-screen))
- Added a Reconnect option when the connection to the gateway is lost
- Added an automatic refresh of devices ([issue #1](https://bitbucket.org/garrcomm/fonsterstod-windows-client/issues/1/automatically-refresh-home-devices-list))

### Fixed

- Performance improvements (cache battery state, don't request all data at all times)
- Minor visual improvements
- Security improvements (UA check implemented ([issue #6](https://bitbucket.org/garrcomm/fonsterstod-windows-client/issues/6/validate-php-desktop-user-agent-string)) and stopped using a CDN for assets)

## [0.1.0-alpha] - 2021-04-24

### Initial release

This is the first release of the app. It includes support for plugs and lights.

[unreleased]: https://bitbucket.org/garrcomm/tradfri-web-application/branches/compare/master%0Dv0.1.2-alpha#diff
[0.1.2-alpha]: https://bitbucket.org/garrcomm/tradfri-web-application/branches/compare/v0.1.2-alpha%0Dv0.1.1-alpha#diff
[0.1.1-alpha]: https://bitbucket.org/garrcomm/tradfri-web-application/branches/compare/v0.1.1-alpha%0Dv0.1.0-alpha#diff
[0.1.0-alpha]: https://bitbucket.org/garrcomm/tradfri-web-application/src/v0.1.0-alpha/
