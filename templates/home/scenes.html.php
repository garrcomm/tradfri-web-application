<?php

/* @var $baseHref string */
/* @var $tradfriScenes \Garrcomm\Tradfri\Model\TradfriScene[] */
?>
<div class="row">
    <?php $i = 0;
    foreach ($tradfriScenes as $tradfriScene) {
        if (++$i == 5) {
            break;
        }
        ?>
        <div class="col-sm-3">
            <a onclick="$('#activatingSceneModal').modal('show');" href="<?= htmlspecialchars($baseHref) ?>scene/activate/<?= htmlspecialchars($tradfriScene->getId()) ?>?redir=<?= htmlspecialchars($baseHref) ?>index/home" class="btn btn-light btn-block">
                <i class="<?= \App\Middleware\Tradfri::sceneIconIndexToClass($tradfriScene->getIconIndex()) ?> fa-lg fa-fw"></i>
                <br>
                <?= htmlspecialchars($tradfriScene->getName()) ?>
            </a>
        </div>
        <?php
    }
    ?>
</div>
<?php if (count($tradfriScenes) > 4) : ?>
<div class="row">
    <div class="col-12 text-center">
        <br>
        <a class="btn btn-primary" href="<?= htmlspecialchars($baseHref) ?>scene">All scenes</a>
        <br>
        &nbsp;
    </div>
</div>
<?php endif; ?>

<!-- Scene loading modal -->
<div class="modal fade" data-keyboard="false" data-backdrop="static" id="activatingSceneModal" tabindex="-1" role="dialog" aria-labelledby="activatingSceneModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="activatingSceneModalLabel">Activating scene...</h5>
            </div>
            <div class="modal-body text-center">
                <div class="spinner-border" role="status">
                    <span class="sr-only">Activating...</span>
                </div>
            </div>
        </div>
    </div>
</div>
