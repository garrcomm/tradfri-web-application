<?php

/* @var $baseHref string */
/* @var $tradfriGroup \Garrcomm\Tradfri\Model\TradfriGroup */

ob_start();

$groupStateOn = false;
$lampCount = 0;
$blindCount = 0;
$plugCount = 0;
$brightnesses = array();
foreach ($tradfriGroup->getDevices() as $device) {
    // Define which template should be loaded for this device
    if ($device instanceof \Garrcomm\Tradfri\Model\TradfriLight) {
        ++$lampCount;
        $groupStateOn = $device->isOn() ? true : $groupStateOn;
        $brightnesses[] = $device->getBrightness();
        require __DIR__ . '/devices/light.html.php';
    } elseif ($device instanceof \Garrcomm\Tradfri\Model\TradfriBlind) {
        ++$blindCount;
        require __DIR__ . '/devices/blind.html.php';
    } elseif ($device instanceof \Garrcomm\Tradfri\Model\TradfriPlug) {
        ++$plugCount;
        $groupStateOn = $device->isOn() ? true : $groupStateOn;
        require __DIR__ . '/devices/plug.html.php';
    }
}
$groupData = ob_get_clean();
$brightnessAverage = $brightnesses ? (array_sum($brightnesses) / count($brightnesses)) : 0;
$brightnessAveragePercent = ceil($brightnessAverage / 254 * 100);

if ($groupData) :
    ?>
    <thead class="thead-light">
    <tr data-group-id="<?= htmlspecialchars($tradfriGroup->getId()) ?>">
        <th class="td-min-width align-middle">
            <?php if ($blindCount == 0) : ?>
                <a data-group-toggle="off" class="<?= ($groupStateOn ? '' : 'd-none') ?> show-when-on" href="<?= htmlspecialchars($baseHref) ?>index/groupOff/<?= htmlspecialchars($tradfriGroup->getId()) ?>"><i class="fas fa-2x fa-toggle-on"></i></a>
                <a data-group-toggle="on" class="<?= ($groupStateOn ? 'd-none' : '') ?> show-when-off" href="<?= htmlspecialchars($baseHref) ?>index/groupOn/<?= htmlspecialchars($tradfriGroup->getId()) ?>"><i class="fas fa-2x fa-toggle-off"></i></a>
            <?php endif; ?>
        </th>
        <th class="text-center align-middle">
            <?= htmlspecialchars($tradfriGroup->getName()) ?>
            <?php if ($blindCount == 0 && $plugCount == 0 && $lampCount > 0) : ?>
                <div class="<?= ($groupStateOn ? '' : 'd-none') ?> show-when-on progress" data-base-href="<?= htmlspecialchars($baseHref) ?>index/groupBrightness/<?= htmlspecialchars($tradfriGroup->getId()) ?>/" title="-" data-toggle="tooltip" data-placement="bottom" style="background: white">
                    <div class="progress-bar clickable" role="progressbar" style="width: <?= $brightnessAveragePercent ?>%" aria-valuenow="<?= $brightnessAverage ?>" aria-valuemin="0" aria-valuemax="254">
                        <?= $brightnessAveragePercent ?>%
                    </div>
                </div>
                <div class="<?= ($groupStateOn ? 'd-none' : '') ?> show-when-off progress" title="Off" data-toggle="tooltip" data-placement="bottom" style="background: white"></div>
            <?php endif; ?>
        </th>
        <th></th>
    </tr>
    </thead>
    <?= $groupData ?>
<?php endif; ?>

