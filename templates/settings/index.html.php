<?php

/* @var $subpage string */

if ($subpage == 'main') {
    require_once __DIR__ . '/main.html.php';
} elseif ($subpage == 'licenses') {
    require_once __DIR__ . '/licenses.html.php';
} elseif ($subpage == 'debug') {
    require_once __DIR__ . '/debug.html.php';
} else {
    throw new \Miniframe\Response\NotFoundResponse();
}
