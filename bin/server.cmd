@echo off
if exist "%~dp0..\win32\php.exe" (
    "%~dp0..\win32\php.exe" -t "%~dp0..\public" -S 127.0.0.1:80
) else (
    docker-compose.exe up
)
