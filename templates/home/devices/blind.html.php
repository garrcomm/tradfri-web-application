<?php

/* @var $baseHref string */
/* @var $device \Garrcomm\Tradfri\Model\TradfriBlind */
?>
<tr
    class="<?= (!$device->isReachable() ? 'text-muted' : '') ?>"
    data-device-type="blind"
    data-device-id="<?= htmlspecialchars($device->getId()) ?>"
>
    <td class="td-min-width align-middle">
        <a href="<?= htmlspecialchars($baseHref) ?>index/position/<?= htmlspecialchars($device->getId()) ?>/0">
            <span class="fa-stack fa-fw">
                <i class="far fa-calendar fa-stack-2x"></i>
                <i class="fas fa-arrow-up fa-stack-1x"></i>
            </span>
        </a>
    </td>
    <td class="text-center align-middle">
        <?= htmlspecialchars($device->getName()) ?>
        <div class="progress" data-base-href="<?= htmlspecialchars($baseHref) ?>index/position/<?= htmlspecialchars($device->getId()) ?>/" title="-" data-toggle="tooltip" data-placement="bottom">
            <div class="progress-bar clickable" role="progressbar" style="width: <?= $device->getPosition() ?>%" aria-valuenow="<?= round($device->getPosition()) ?>" aria-valuemin="0" aria-valuemax="254">
                <?= round($device->getPosition()) ?>%
            </div>
        </div>
    </td>
    <td class="text-right td-min-width align-middle">
        <a href="<?= htmlspecialchars($baseHref) ?>index/position/<?= htmlspecialchars($device->getId()) ?>/100">
            <span class="fa-stack fa-fw">
                <i class="fas fa-calendar fa-stack-2x"></i>
                <i class="fas fa-arrow-down fa-inverse fa-stack-1x" style="padding-top: 20%"></i>
            </span>
        </a>
    </td>
</tr>
