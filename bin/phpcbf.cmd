@echo off

rem Validate if PHP Code Sniffer is installed
if not exist "%~dp0..\vendor\squizlabs\php_codesniffer\bin\phpcbf" (
    echo Use "composer install" to install PHP Code Sniffer first
    goto :eof
)

rem If php.exe exists, use the Windows build, otherwise try to use Docker
if exist "%~dp0..\win32\php.exe" (
    "%~dp0..\win32\php.exe" -f "%~dp0..\vendor\squizlabs\php_codesniffer\bin\phpcbf" -- %*
) else (
    docker-compose.exe exec php php vendor/squizlabs/php_codesniffer/bin/phpcbf %*
)
