<?php

namespace App\Middleware;

use Miniframe\Core\AbstractMiddleware;
use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\Response\ForbiddenResponse;

class UserAgentAccess extends AbstractMiddleware
{
    /**
     * Blocks connections based on user agents
     *
     * @param Request $request Reference to the Request object.
     * @param Config  $config  Reference to the Config object.
     */
    public function __construct(Request $request, Config $config)
    {
        parent::__construct($request, $config);

        // Fetch the user agent
        $userAgent = $request->getServer('HTTP_USER_AGENT');
        if (!$userAgent) {
            throw new ForbiddenResponse();
        }

        // Walk through the allow list
        $allow = (array)$config->get('useragentaccess', 'allow');
        foreach ($allow as $userAgentAllowed) {
            if (substr($userAgentAllowed, 0, 1) == '/' && preg_match($userAgentAllowed, $userAgent)) {
                return;
            }
            if (strtolower($userAgent) === strtolower($userAgentAllowed)) {
                return;
            }
        }

        // Walk through the deny list
        if (!$config->has('useragentaccess', 'deny')) {
            throw new ForbiddenResponse();
        }
        $deny = (array)$config->get('useragentaccess', 'deny');
        foreach ($deny as $userAgentDenied) {
            if (substr($userAgentDenied, 0, 1) == '/' && preg_match($userAgentDenied, $userAgent)) {
                throw new ForbiddenResponse();
            }
            if (strtolower($userAgent) === strtolower($userAgentDenied)) {
                throw new ForbiddenResponse();
            }
        }
    }
}
