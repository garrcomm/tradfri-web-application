<?php

/* @var $baseHref string */
/* @var $brightness int */
/* @var $device \Garrcomm\Tradfri\Model\TradfriLight */
?>
<!-- Modal -->
<div class="modal fade" id="lampmodal<?= htmlspecialchars($device->getId()) ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">
                    <a data-device-toggle="off" class="<?= ($device->isOn() ? '' : 'd-none') ?> show-when-on" href="<?= htmlspecialchars($baseHref) ?>index/off/<?= htmlspecialchars($device->getId()) ?>"><i class="fas fa-toggle-on"></i></a>
                    <a data-device-toggle="on" class="<?= ($device->isOn() ? 'd-none' : '') ?> show-when-off" href="<?= htmlspecialchars($baseHref) ?>index/on/<?= htmlspecialchars($device->getId()) ?>"><i class="fas fa-toggle-off"></i></a>
                    <?= htmlspecialchars($device->getName()) ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <?php if (method_exists($device, 'getRgbColor')) : ?>
                            <?php foreach ($device->getAvailableRgbColors() as $colorCode => $colorName) : ?>
                                <a class="<?= ($device->isOn() ? '' : 'd-none') ?> show-when-on" data-device-color="<?= htmlspecialchars($colorCode) ?>" data-toggle="tooltip" data-placement="bottom" href="<?= htmlspecialchars($baseHref) ?>index/color/<?= htmlspecialchars($device->getId()) ?>/<?= htmlspecialchars($colorCode) ?>" title="<?= htmlspecialchars($colorName) ?>"><i class="fas fa-circle fa-fw fa-4x" style="color: #<?= htmlspecialchars($colorCode) ?>"></i></a>
                                <a class="<?= ($device->isOn() ? 'd-none' : '') ?> show-when-off"><i class="far fa-circle fa-fw fa-4x" style="color: #dddddd"></i></a>
                                <?php
                                $i = isset($i) ? $i + 1 : 1;
                                if ($i == 5) {
                                    $i = 0;
                                    echo '<br style="margin-bottom: 8pt">';
                                }
                                ?>
                            <?php endforeach ; ?>
                        <?php else : ?>
                            <i class="<?= ($device->isOn() ? '' : 'd-none') ?> show-when-on fas fa-circle fa-fw fa-4x" style="color: #f1e0b5"></i>
                            <i class="<?= ($device->isOn() ? 'd-none' : '') ?> show-when-off far fa-circle fa-fw fa-4x" style="color: #dddddd"></i>
                        <?php endif; ?>
                    </div>
                </div>
                <br>
                <div class="<?= ($device->isOn() ? '' : 'd-none') ?> show-when-on progress" data-base-href="<?= htmlspecialchars($baseHref) ?>index/brightness/<?= htmlspecialchars($device->getId()) ?>/" title="-" data-toggle="tooltip" data-placement="bottom">
                    <div class="progress-bar clickable" role="progressbar" style="width: <?= $brightness ?>%" aria-valuenow="<?= $device->getBrightness() ?>" aria-valuemin="0" aria-valuemax="254">
                        <?= round($brightness) ?>%
                    </div>
                </div>
                <div class="<?= ($device->isOn() ? 'd-none' : '') ?> show-when-off progress" title="Off" data-toggle="tooltip" data-placement="bottom"></div>
            </div>
        </div>
    </div>
</div>
