<?php

namespace App\Middleware;

use App\Service\FsTools;
use Garrcomm\Netutils\Model\Ipv4Address;
use Garrcomm\Netutils\Model\MacAddress;
use Garrcomm\Netutils\Service\IpTools;
use Garrcomm\Tradfri\Exception\TradfriException;
use Garrcomm\Tradfri\Service\Tradfri as TradfriService;
use Miniframe\Core\AbstractMiddleware;
use Miniframe\Core\Config;
use Miniframe\Core\Request;
use Miniframe\Core\Response;
use Miniframe\Response\InternalServerErrorResponse;
use Miniframe\Response\RedirectResponse;

class Tradfri extends AbstractMiddleware
{
    /**
     * Reference to the TRÅDFRI service
     *
     * @var TradfriService
     */
    private $service;

    /**
     * Path to the coap-client
     *
     * @var string
     */
    private $coapPath;

    /**
     * This middleware helps locating the gateway and configuring the TRÅDFRI library.
     *
     * @param Request $request Reference to the Request object.
     * @param Config  $config  Reference to the Config object.
     */
    public function __construct(Request $request, Config $config)
    {
        parent::__construct($request, $config);
        if ($config->has('tradfri', 'coap-client')) {
            $this->coapPath = $config->getPath('tradfri', 'coap-client');
        } elseif (PHP_OS == 'WINNT') {
            $this->coapPath = (new FsTools())->which('coap-client.exe');
        } else {
            $this->coapPath = (new FsTools())->which('coap-client');
        }
        if (!$this->coapPath) {
            throw new \RuntimeException('coap-client not found');
        }
    }


    /**
     * Returns the version of libcoap
     *
     * @return string|null
     */
    public function getLibCoapVersion(): ?string
    {
        exec($this->coapPath . ' 2>&1', $output);
        preg_match('/v[0-9\.a-z]+/i', $output[0], $matches);
        return $matches ? $matches[0] : 'unknown';
    }

    /**
     * Reads the secrets data (if present) and returns an array
     * with keys 'serialNumber', 'gatewayIp', 'securityCode', 'clientIdentity' and 'privateSharedKey'
     *
     * @return string[]|null[]
     */
    private function getSecretsData(): array
    {
        $secretsFile = $this->config->getPath('tradfri', 'gateway_secrets_file');
        if (file_exists($secretsFile)) {
            $data = json_decode(file_get_contents($secretsFile), true, 512, JSON_THROW_ON_ERROR);
        } else {
            $data = array(
                'serialNumber'     => null,
                'gatewayIp'        => null,
                'securityCode'     => null,
                'clientIdentity'   => null,
                'privateSharedKey' => null,
            );
        }
        return $data;
    }

    /**
     * Stores secrets into the secrets file. Overwrites all values, even when set to null!
     *
     * @param string|null $serialNumber   The serial number.
     * @param string|null $gatewayIp      The Gateway IP.
     * @param string|null $securityCode   The security code.
     * @param string|null $clientIdentity The client identity.
     * @param string|null $privateKey     The private key.
     *
     * @return void
     */
    private function setSecretsData(
        ?string $serialNumber,
        ?string $gatewayIp,
        ?string $securityCode,
        ?string $clientIdentity,
        ?string $privateKey
    ): void {
        $secretsFile = $this->config->getPath('tradfri', 'gateway_secrets_file');
        $data = array(
            'serialNumber'     => $serialNumber,
            'gatewayIp'        => $gatewayIp,
            'securityCode'     => $securityCode,
            'clientIdentity'   => $clientIdentity,
            'privateSharedKey' => $privateKey,
        );
        file_put_contents($secretsFile, json_encode($data, JSON_PRETTY_PRINT));
    }

    /**
     * Initializes the TRÅDFRI Service and returns a reference to it
     *
     * @return TradfriService
     */
    public function getService(): TradfriService
    {
        if ($this->service) {
            return $this->service;
        }

        // Reads the config
        $data = $this->getSecretsData();

        // When no gateway IP is known, redirect to the connect page
        if ($data['gatewayIp'] === null) {
            throw $this->redirectToPage('connect');
        }

        // When no credentials are known, also redirect to the connect page
        if ($data['clientIdentity'] === null || $data['privateSharedKey'] === null) {
            throw $this->redirectToPage('connect');
        }

        // Initializes the service
        $tradfriService = new TradfriService(
            $data['gatewayIp'],
            $this->coapPath
        );
        $tradfriService->setClientIdentity($data['clientIdentity'], $data['privateSharedKey']);
        $this->service = $tradfriService;

        return $this->service;
    }

    /**
     * Look for the gateway based on the known serial number
     *
     * @return boolean
     */
    private function relocateByMac(): bool
    {
        $data = $this->getSecretsData();

        // Do we have a serial number?
        if (!isset($data['serialNumber'])) {
            return false;
        }

        // Can we find an IP based on the serial number?
        $ip = (new IpTools())->getIpByMac(new MacAddress($data['serialNumber']));
        if (!$ip) {
            return false;
        }

        // Is the IP different from the one we already know?
        if ($ip->getIpAddress() === $data['gatewayIp']) {
            return false;
        }

        // Store new data
        $this->setSecretsData(
            $data['serialNumber'],
            $ip->getIpAddress(),
            $data['securityCode'],
            $data['clientIdentity'],
            $data['privateSharedKey'],
        );

        return true;
    }

    /**
     * Handles authentication. Always requires a security code, and a serial number or IP address.
     *
     * @param string|null $serialNumber The serial number of the Gateway.
     * @param string      $securityCode The security code of the Gateway.
     * @param string|null $ipAddress    The IP address of the gateway.
     *
     * @return Response
     */
    public function authenticate(?string $serialNumber, string $securityCode, ?string $ipAddress): Response
    {
        if (!$serialNumber && !$ipAddress) {
            return $this->redirectToPage(
                'connect',
                'Please fill in one of Serial Number or IP Address'
            );
        }

        // Locate the device based on it's serial number
        if (!$ipAddress) {
            try {
                $mac = new MacAddress($serialNumber);
            } catch (\InvalidArgumentException $exception) {
                return $this->redirectToPage('connect', 'Please fill in a valid Serial Number');
            }
            $ip = (new IpTools())->getIpByMac($mac);
            if ($ip === null) {
                return $this->redirectToPage(
                    'connect/byip',
                    'Serial number not found. Try using the Gateway\'s IP address'
                );
            }
            $ipAddress = $ip->getIpAddress();
        }

        // Try to get the serial number
        if (!$serialNumber) {
            try {
                $mac = (new IpTools())->getMacByIp(new Ipv4Address($ipAddress));
            } catch (\Exception $exception) {
                $mac = null; // ignore; we don't NEED the serial number, it's a nice-to-have
            }
            if ($mac) {
                $serialNumber = $mac->format('-', MacAddress::LOWERCASE);
            }
        }

        // Authorize
        try {
            $tradfriService = new TradfriService(
                $ipAddress,
                $this->coapPath
            );
        } catch (TradfriException $exception) {
            if ($exception->getCode() == TradfriException::INVALID_IPV4_ADDRESS) {
                return $this->redirectToPage('connect', 'Please fill in a valid IP address');
            }
            throw $exception;
        }
        $credentials = $tradfriService->authenticate($securityCode);

        // Store credentials
        $this->setSecretsData(
            $serialNumber,
            $ipAddress,
            $securityCode,
            $credentials['clientIdentity'],
            $credentials['privateSharedKey']
        );

        return $this->redirectToPage();
    }

    /**
     * Builds a redirect response
     *
     * @param string      $page    Page to redirect to; when empty, redirect to homepage.
     * @param string|null $message Additionally we can add a message to the URL.
     *
     * @return RedirectResponse
     */
    private function redirectToPage(string $page = '', string $message = null): RedirectResponse
    {
        $fullUrl = $this->config->get('framework', 'base_href')
            . $page
            . ($message ? '?m=' . rawurlencode(base64_encode($message)) : '');
        return new RedirectResponse($fullUrl, RedirectResponse::FOUND);
    }

    /**
     * Catches internal server errors related to the TRÅDFRI service
     *
     * @return callable[]
     */
    public function getPostProcessors(): array
    {
        return [function (Response $response, bool $thrown): Response {
            if (
                $response instanceof InternalServerErrorResponse
                && is_object($response->getPrevious())
                && $response->getPrevious() instanceof TradfriException
                && $response->getPrevious()->getCode() == TradfriException::CONNECTION_FAILED
            ) {
                if ($this->relocateByMac()) {
                    // Reload page for simple GET requests
                    if ($this->request->getServer('REQUEST_METHOD') == 'GET') {
                        return new RedirectResponse(
                            $this->request->getServer('REQUEST_URI'),
                            RedirectResponse::FOUND
                        );
                    }
                    // For other requests, suggest reloading
                    return new Response('Connection lost, press F5 to reconnect');
                }
                return $this->redirectToPage('connect/failed');
            }
            return $response;
        }];
    }

    /**
     * Disconnects the app from the TRÅDFRI Gateway
     *
     * @return void
     */
    public function disconnect(): void
    {
        $this->setSecretsData(null, null, null, null, null);
        $batteryCacheFile = $this->config->getPath('tradfri', 'battery_cache_file');
        if (file_exists($batteryCacheFile)) {
            unlink($batteryCacheFile);
        }
    }

    /**
     * Returns a HTML class representation of an icon index
     *
     * @param integer $index The icon index.
     *
     * @return string
     */
    public static function sceneIconIndexToClass(int $index): string
    {
        switch ($index) {
            case 1:
                return 'mdi mdi-desk-lamp';
            case 2:
                return 'mdi mdi-weather-night';
            case 3:
                return 'mdi mdi-wall-sconce-round';
            case 4:
                return 'mdi mdi-party-popper';
            case 5:
                return 'mdi mdi-pot-steam-outline';
            case 6:
                return 'mdi mdi-beer-outline';
            case 7:
                return 'mdi mdi-book-open-page-variant';
            case 8:
                return 'mdi mdi-weather-partly-cloudy';
            case 9:
                return 'mdi mdi-ceiling-light';
            case 10:
                return 'mdi mdi-silverware-fork-knife';
            case 12:
                return 'mdi mdi-television-classic';
            case 13:
                return 'mdi mdi-briefcase-variant-outline';
            case 14:
                return 'mdi mdi-table-chair';
            case 15:
                return 'mdi mdi-sprout-outline';
            case 16:
                return 'mdi mdi-cake-variant';
            case 17:
                return 'mdi mdi-crystal-ball mdi-rotate-180';
            case 18:
                return 'mdi mdi-coffee-outline';
            case 19:
                return 'mdi mdi-bed-outline';
            case 20:
                return 'mdi mdi-popcorn';
            case 21:
                return 'mdi mdi-panda';
            case 22:
                return 'mdi mdi-tea-outline';
            default:
                return 'mdi mdi-power';
        }
    }
}
