@echo off

rem Validate if PHP and Composer are available in the win32 folder
if exist "%~dp0..\win32\composer.phar" (
    if exist "%~dp0..\win32\php.exe" (
        "%~dp0..\win32\php.exe" -f "%~dp0..\win32\composer.phar" %*
        goto :eof
    )
)

rem If not, use docker
docker-compose.exe exec php composer %*
