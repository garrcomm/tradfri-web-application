<?php

/* @var $baseHref string */
/* @var $page string */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Ikea TRÅDFRI</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?= htmlspecialchars($baseHref) ?>assets/fontawesome-free-5.15.3-web/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= htmlspecialchars($baseHref) ?>assets/bootstrap-4.4.1-dist/css/bootstrap.min.css">
    <link href="<?= htmlspecialchars($baseHref) ?>assets/MaterialDesign-Webfont-master/css/materialdesignicons.min.css" media="all" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .td-min-width {
            width: 1px;
            white-space: nowrap;
        }
        body {
            cursor: default;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-3">
            <div class="sticky-top">
                <?php require __DIR__ . '/nav.html.php'; ?>
                <?php require __DIR__ . '/battery_status.html.php'; ?>
            </div>
        </div>
        <div class="col-9">
            <?php
            switch ($page) {
                case 'home':
                    require __DIR__ . '/home/index.html.php';
                    break;
                case 'settings':
                    require __DIR__ . '/settings/index.html.php';
                    break;
                case 'scene':
                    require __DIR__ . '/scene/index.html.php';
                    break;
                default:
                    throw new \Miniframe\Response\NotFoundResponse();
            }
            ?>
        </div>
    </div>
</div>
<script src="<?= htmlspecialchars($baseHref) ?>assets/jquery-3.6.0/jquery-3.6.0.min.js"></script>
<script src="<?= htmlspecialchars($baseHref) ?>assets/bootstrap-4.4.1-dist/js/bootstrap.bundle.min.js"></script>
<?php if ($page == 'home') : ?>
    <script src="<?= htmlspecialchars($baseHref) ?>assets/group.js"></script>
    <script src="<?= htmlspecialchars($baseHref) ?>assets/plug.js"></script>
    <script src="<?= htmlspecialchars($baseHref) ?>assets/light.js"></script>
    <script src="<?= htmlspecialchars($baseHref) ?>assets/blind.js"></script>
    <script src="<?= htmlspecialchars($baseHref) ?>assets/autorefresh.js"></script>
<?php endif; ?>
<script type="text/javascript">
    $(function () {
        // Generic tooltips enabled
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
</body>
</html>
