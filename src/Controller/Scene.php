<?php

namespace App\Controller;

use Miniframe\Core\Response;
use Miniframe\Response\NotFoundResponse;
use Miniframe\Response\PhpResponse;
use Miniframe\Response\RedirectResponse;

class Scene extends AbstractTradfriEnabledController
{
    /**
     * This method responds to `/scene`
     *
     * @return Response
     */
    public function main(): Response
    {
        return new PhpResponse(__DIR__ . '/../../templates/loading.html.php', [
            'baseHref' => $this->config->get('framework', 'base_href'),
            'page'     => 'scene',
            'redirect' => 'scene/home',
        ]);
    }

    /**
     * This method responds to `/scene/home` and shows all scenes
     *
     * @return Response
     */
    public function home(): Response
    {
        return new PhpResponse(__DIR__ . '/../../templates/index.html.php', [
            'baseHref'       => $this->config->get('framework', 'base_href'),
            'page'           => 'scene',
            'tradfriGroups'  => $this->listGroups(),
            'tradfriDevices' => $this->listDevices(),
            'tradfriScenes'  => $this->listScenes(),
        ]);
    }

    /**
     * This method responds to `/scene/activate` and executes a scene
     *
     * @return Response
     */
    public function activate(): Response
    {
        $sceneId = $this->request->getPath(2);
        if (!$sceneId || !$scene = $this->tradfriService->getScene($sceneId)) {
            throw new NotFoundResponse();
        }

        $scene->execute();

        // Redirects to the redirect URL
        $redir = $this->request->getRequest('redir') ?? $this->config->get('framework', 'base_href') . 'scene';
        return new RedirectResponse($redir);
    }
}
