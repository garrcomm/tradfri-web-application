<?php

/* @var $baseHref string */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Ikea TRÅDFRI</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?= htmlspecialchars($baseHref) ?>assets/fontawesome-free-5.15.3-web/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= htmlspecialchars($baseHref) ?>assets/bootstrap-4.4.1-dist/css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-xl-7 col-lg-8">
            <h2>Can't connect to IKEA TRÅDFRI Gateway</h2>
            <p>
                The connection to the TRÅDFRI Gateway has been lost. This can mean that your computer is not connected to the same network,
                or your gateway isn't connected to the network.
                If that's not the case, you may want to disconnect the app and reconnect to your gateway again.
            </p>
            <p>
                <a href="<?= htmlspecialchars($baseHref) ?>" class="btn btn-primary">Try again</a>
                <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#disconnectModal">Disconnect app</button>
            </p>
        </div>
    </div>
</div>
<?php include __DIR__ . '/disconnect_modal.html.php'; ?>
<script src="<?= htmlspecialchars($baseHref) ?>assets/jquery-3.6.0/jquery-3.6.0.min.js"></script>
<script src="<?= htmlspecialchars($baseHref) ?>assets/bootstrap-4.4.1-dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
