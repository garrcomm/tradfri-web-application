function autoRefresh() {
    $.ajax({url: $('[data-refresh-url]').data('refresh-url'), data: { ajax: 1 }, success: function(data) {
        for (deviceId in data) {
            if ($('[data-device-id=' + deviceId + ']').data('device-type') == 'plug') {
                refreshPlug(data[deviceId]);
            }
            if ($('[data-device-id=' + deviceId + ']').data('device-type') == 'light') {
                refreshLight(data[deviceId]);
            }
            if ($('[data-device-id=' + deviceId + ']').data('device-type') == 'blind') {
                refreshBlind(data[deviceId]);
            }
        }

        $('thead [data-group-id]').each(function() {
            updateGroupHtml($(this).data('group-id'));
        });

        // Only set the timeout again after success
        window.setTimeout(autoRefresh, parseInt($('[data-refresh-timeout]').data('refresh-timeout'), 10));
    }});
}

window.setTimeout(autoRefresh, parseInt($('[data-refresh-timeout]').data('refresh-timeout'), 10));
