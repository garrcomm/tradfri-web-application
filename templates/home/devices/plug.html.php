<?php

/* @var $baseHref string */
/* @var $device \Garrcomm\Tradfri\Model\TradfriPlug */
/* @var $tradfriGroup \Garrcomm\Tradfri\Model\TradfriGroup */
?>
<tr
    class="<?= (!$device->isReachable() ? 'text-muted' : '') ?>"
    data-device-type="plug"
    data-device-id="<?= htmlspecialchars($device->getId()) ?>"
    data-group-id="<?= htmlspecialchars($tradfriGroup->getId()) ?>"
>
    <td class="td-min-width align-middle">
        <a data-device-toggle="off" class="<?= ($device->isOn() ? '' : 'd-none') ?> show-when-on" href="<?= htmlspecialchars($baseHref) ?>index/off/<?= htmlspecialchars($device->getId()) ?>"><i class="fas fa-2x fa-plug fa-fw"></i></a>
        <a data-device-toggle="on" class="<?= ($device->isOn() ? 'd-none' : '') ?> show-when-off" href="<?= htmlspecialchars($baseHref) ?>index/on/<?= htmlspecialchars($device->getId()) ?>"><i class="fas fa-2x fa-plug fa-fw"></i></a>
    </td>
    <td class="text-center align-middle">
        <?= htmlspecialchars($device->getName()) ?>
    </td>
    <td class="text-right td-min-width align-middle">
        <a data-device-toggle="off" class="<?= ($device->isOn() ? '' : 'd-none') ?> show-when-on" href="<?= htmlspecialchars($baseHref) ?>index/off/<?= htmlspecialchars($device->getId()) ?>"><i class="fas fa-2x fa-toggle-on"></i></a>
        <a data-device-toggle="on" class="<?= ($device->isOn() ? 'd-none' : '') ?> show-when-off" href="<?= htmlspecialchars($baseHref) ?>index/on/<?= htmlspecialchars($device->getId()) ?>"><i class="fas fa-2x fa-toggle-off"></i></a>
    </td>
</tr>
