<?php

/* @var $baseHref string */
/* @var $input string */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Ikea TRÅDFRI</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?= htmlspecialchars($baseHref) ?>assets/fontawesome-free-5.15.3-web/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= htmlspecialchars($baseHref) ?>assets/bootstrap-4.4.1-dist/css/bootstrap.min.css">
</head>
<body>
<div class="modal fade" data-keyboard="false" data-backdrop="static" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="loadingModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="loadingModalLabel">Connecting to TRÅDFRI Gateway</h5>
            </div>
            <div class="modal-body text-center">
                <div class="spinner-border" role="status">
                    <span class="sr-only">Connecting...</span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-xl-7 col-lg-8">
            <h2>Connect to your IKEA TRÅDFRI Gateway</h2>
            <p>
                <?php if ($input == 'sn') : ?>
                    Please fill in the details below to connect to your IKEA TRÅDFRI Gateway.
                    In some network configurations, we can't find the gateway by it's serial number.
                    If that's the case, please <a href="<?= htmlspecialchars($baseHref) ?>connect/byip">connect by IP</a>.
                <?php elseif ($input == 'ip') : ?>
                    Please fill in the details below to connect to your IKEA TRÅDFRI Gateway.
                    Only in special cases, we want to reach the gateway by it's IP address.
                    Preferably we want to detect the Gateway by it's serial number.
                    If you want to try that, please <a href="<?= htmlspecialchars($baseHref) ?>connect">connect by Serial Number</a>.
                <?php endif; ?>
            </p>
            <?php if (isset($_GET['m'])) : ?>
                <div class="alert alert-danger" role="alert">
                    <?= htmlspecialchars(base64_decode($_GET['m'])) ?>
                </div>
            <?php endif; ?>
            <form method="post" action="<?= htmlspecialchars($baseHref) ?>connect">
                <div class="form-group">
                    <?php if ($input == 'sn') : ?>
                        <label for="serialNumber">Serial Number</label>
                        <input type="text" class="form-control" id="serialNumber" name="sn" aria-describedby="snHelp">
                        <small id="sn" class="form-text text-muted">The Serial Number is printed on the bottom of your gateway.</small>
                    <?php elseif ($input == 'ip') : ?>
                        <label for="ipAddress">IP address</label>
                        <input type="text" class="form-control" id="ipAddress" name="ip" aria-describedby="ipHelp">
                        <small id="ip" class="form-text text-muted">The IP address can be found in your router or DHCP server.</small>
                    <?php endif; ?>
                </div>
                <div class="form-group">
                    <label for="securityCode">Security Code</label>
                    <input type="text" class="form-control" id="securityCode" name="sc" aria-describedby="sc">
                    <small id="sc" class="form-text text-muted">The Security Code is printed on the bottom of your gateway.</small>
                </div>
                <button type="submit" class="btn btn-primary">Connect</button>
            </form>
        </div>
    </div>
</div>
<script src="<?= htmlspecialchars($baseHref) ?>assets/jquery-3.6.0/jquery-3.6.0.min.js"></script>
<script src="<?= htmlspecialchars($baseHref) ?>assets/bootstrap-4.4.1-dist/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript">
    $(function() {
        $('form').submit(function() {
            $('#loadingModal').modal('show');
            return true;
        });
    });
</script>
</body>
</html>
