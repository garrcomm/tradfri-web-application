// Toggle buttons for groups clickable
$('[data-group-toggle]').click(function(e) {
    // Execute call
    $.ajax({url: $(this).attr('href'), data: { ajax: 1 }, success: function(data) {
        refreshGroup(data);
    }});

    // Cancel click event
    e.preventDefault();
    return false;
});

// Brightness bars hoverable
$('thead [data-group-id] .progress-bar.clickable').parent().mousemove(function(event) {
    let positionOverElement = (event.pageX - $(this).offset().left);
    let percentage = 100 / $(this).width() * positionOverElement;
    $(this).attr('data-original-title', 'Click to set to ' + Math.ceil(percentage) + '%');
    $(this).tooltip('update').tooltip('show');
});

// Brightness bars clickable
$('thead [data-group-id] .progress-bar.clickable').parent().click(function(event) {
    let positionOverElement = (event.pageX - $(this).offset().left);
    let newValue = 254 / $(this).width() * positionOverElement;
    let href = $(this).data('base-href') + Math.ceil(newValue);

    // Execute call
    $.ajax({url: href, data: { ajax: 1 }, success: function(data) {
        refreshGroup(data);
    }});
});

// Show a pointer to make sure it looks clickable
$('thead [data-group-id] .progress-bar.clickable').parent().css('cursor', 'pointer');


function refreshGroup(data) {
    for (deviceId in data.devices) {
        if ($('[data-device-id=' + deviceId + ']').data('device-type') == 'plug') {
            refreshPlug(data.devices[deviceId]);
        }
        if ($('[data-device-id=' + deviceId + ']').data('device-type') == 'light') {
            refreshLight(data.devices[deviceId]);
        }
    }
    updateGroupHtml(data.id);
}

function updateGroupHtml(groupId) {
    var onOff = false;
    var brightnessSum = 0;
    var brightnessCount = 0;
    var groupElement = $('thead [data-group-id=' + groupId + ']');
    $('[data-group-id=' + groupId + '][data-device-id]').each(function() {
        if ($(this).find('[data-device-toggle].d-none').data('device-toggle') == 'on') {
            onOff = true;
        }
        brightnessSum += parseInt($(this).find('.progress-bar.clickable').text(), 10);
        brightnessCount++;
    });

    // Set brightness
    var brightnessAverage = Math.ceil(brightnessSum / brightnessCount);
    groupElement.find('.progress-bar.clickable').css('width', brightnessAverage + '%');
    groupElement.find('.progress-bar.clickable').text(brightnessAverage + '%');

    // Set on/off
    if (onOff) {
        groupElement.find('.show-when-on').removeClass('d-none');
        groupElement.find('.show-when-off').addClass('d-none');
    } else {
        groupElement.find('.show-when-on').addClass('d-none');
        groupElement.find('.show-when-off').removeClass('d-none');
    }
}
