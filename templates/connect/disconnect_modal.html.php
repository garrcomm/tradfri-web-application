<?php

/* @var $baseHref string */
?>
<!-- Disconnect modal dialog -->
<div class="modal fade" id="disconnectModal" tabindex="-1" role="dialog" aria-labelledby="disconnectModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="disconnectModalLabel">Disconnect from your gateway</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Disconnecting this app from your current gateway makes it possible to connect to another gateway.
                This app only connects to one gateway at the same time, just like the mobile app.<br>
                <br>
                All settings are stored on the gateway, so pressing "Disconnect" won't remove any settings on the gateway.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <a href="<?= $baseHref ?>connect/disconnect" class="btn btn-danger">Disconnect</a>
            </div>
        </div>
    </div>
</div>
