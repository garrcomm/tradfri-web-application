<?php

/* @var $baseHref string */
/* @var $device \Garrcomm\Tradfri\Model\TradfriLight */
/* @var $tradfriGroup \Garrcomm\Tradfri\Model\TradfriGroup */

$brightness = $device->getBrightness() / 254 * 100;
?>
<tr
    class="<?= (!$device->isReachable() ? 'text-muted' : '') ?>"
    data-device-type="light"
    data-device-id="<?= htmlspecialchars($device->getId()) ?>"
    data-group-id="<?= htmlspecialchars($tradfriGroup->getId()) ?>"
>
    <td class="td-min-width align-middle">
        <a data-device-toggle="off" class="<?= ($device->isOn() ? '' : 'd-none') ?> show-when-on" href="<?= htmlspecialchars($baseHref) ?>index/off/<?= htmlspecialchars($device->getId()) ?>"><i class="fas fa-lightbulb fa-fw fa-2x"></i></a>
        <a data-device-toggle="on" class="<?= ($device->isOn() ? 'd-none' : '') ?> show-when-off" href="<?= htmlspecialchars($baseHref) ?>index/on/<?= htmlspecialchars($device->getId()) ?>"><i class="far fa-lightbulb fa-fw fa-2x"></i></a>
    </td>
    <td class="text-center align-middle">
        <?= htmlspecialchars($device->getName()) ?>
        <div class="<?= ($device->isOn() ? '' : 'd-none') ?> show-when-on progress" data-base-href="<?= htmlspecialchars($baseHref) ?>index/brightness/<?= htmlspecialchars($device->getId()) ?>/" title="-" data-toggle="tooltip" data-placement="bottom">
            <div class="progress-bar clickable" role="progressbar" style="width: <?= $brightness ?>%" aria-valuenow="<?= $device->getBrightness() ?>" aria-valuemin="0" aria-valuemax="254">
                <?= ceil($brightness) ?>%
            </div>
        </div>
        <div class="<?= ($device->isOn() ? 'd-none' : '') ?> show-when-off progress" title="Off" data-toggle="tooltip" data-placement="bottom"></div>
        <?php require __DIR__ . '/light_modal.html.php'; ?>
    </td>
    <td class="text-right td-min-width align-middle">
        <a data-toggle="modal" data-target="#lampmodal<?= htmlspecialchars($device->getId()) ?>" href="#">
            <i class="<?= ($device->isOn() ? '' : 'd-none') ?> show-when-on fas fa-circle fa-2x show-color" style="color: #<?= htmlspecialchars(method_exists($device, 'getRgbColor') ? $device->getRgbColor() : 'f1e0b5') ?>"></i>
            <i class="<?= ($device->isOn() ? 'd-none' : '') ?> show-when-off far fa-circle fa-2x" style="color: #dddddd"></i>
        </a>
    </td>
</tr>
