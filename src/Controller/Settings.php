<?php

namespace App\Controller;

use App\Middleware\Tradfri;
use App\Service\FsTools;
use Garrcomm\Tradfri\Model\BaseTradfriDevice;
use Garrcomm\Tradfri\Model\TradfriBlind;
use Garrcomm\Tradfri\Model\TradfriLight;
use Garrcomm\Tradfri\Model\TradfriPlug;
use Garrcomm\Tradfri\Model\TradfriRemote;
use Miniframe\Core\Registry;
use Miniframe\Core\Response;
use Miniframe\Response\PhpResponse;

class Settings extends AbstractTradfriEnabledController
{
    /**
     * This method responds to `/settings`
     *
     * @return Response
     */
    public function main(): Response
    {
        return new PhpResponse(__DIR__ . '/../../templates/loading.html.php', [
            'baseHref' => $this->config->get('framework', 'base_href'),
            'page'     => 'settings',
            'redirect' => 'settings/home',
        ]);
    }

    /**
     * This method responds to `/settings/home`
     *
     * @return Response
     */
    public function home(): Response
    {
        return new PhpResponse(__DIR__ . '/../../templates/index.html.php', [
            'baseHref'              => $this->config->get('framework', 'base_href'),
            'page'                  => 'settings',
            'subpage'               => 'main',
            'tradfriGateway'        => $this->getGateway(),
            'tradfriGroupedDevices' => $this->listDevicesGrouped(),
        ]);
    }

    /**
     * This method responds to `/settings/debug`
     *
     * @return Response
     */
    public function debug(): Response
    {
        return new PhpResponse(__DIR__ . '/../../templates/index.html.php', [
            'baseHref'       => $this->config->get('framework', 'base_href'),
            'page'           => 'settings',
            'subpage'        => 'debug',
            'tradfriGateway' => $this->getGateway(),
            'tradfriGroups'  => $this->listGroups(),
            'tradfriScenes'  => $this->listScenes(),
            'tradfriDevices' => $this->listDevices(),
        ]);
    }

    /**
     * This method responds to `/settings/licenses`
     *
     * @return Response
     */
    public function licenses(): Response
    {
        $dependencies = array(
            [
                'PHP',
                'https://www.php.net/',
                phpversion(),
                'PHP License v3.01',
                'https://www.php.net/license/3_01.txt',
                'fab fa-php',
            ],
        );
        if ($libcoapVersion = Registry::get(Tradfri::class)->getLibCoapVersion()) {
            $dependencies[] = [
                'libcoap',
                'https://libcoap.net/',
                $libcoapVersion,
                'License information',
                'https://raw.githubusercontent.com/obgm/libcoap/develop/LICENSE',
                'fas fa-terminal',
            ];
        };

        // Add other dependencies
        $dependencies = array_merge(
            $dependencies,
            $this->getComposerVersions(),
            $this->getAssetsVersions()
        );

        // Since I also bundle this together with PHP Desktop in another repository,
        // show it as dependency when it exists as well
        if ($desktopVersion = $this->getDesktopVersion()) {
            array_unshift($dependencies, [
                'PHP Desktop',
                'https://github.com/cztomczak/phpdesktop',
                'v' . $desktopVersion,
                'License information',
                'https://raw.githubusercontent.com/cztomczak/phpdesktop/master/src/license.txt',
                'fab fa-chrome',
            ]);
        }

        return new PhpResponse(__DIR__ . '/../../templates/index.html.php', [
            'baseHref'       => $this->config->get('framework', 'base_href'),
            'page'           => 'settings',
            'subpage'        => 'licenses',
            'dependencies'   => $dependencies,
        ]);
    }

    /**
     * Reads the composer.lock file for all dependencies and their version+license
     *
     * @return array
     */
    private function getComposerVersions(): array
    {
        $return = array();

        $lockFile = __DIR__ . '/../../composer.lock';
        $lockData = json_decode(file_get_contents($lockFile), true);
        foreach ($lockData['packages'] as $package) {
            $return[] = [
                $package['name'],
                $package['homepage'] ?? null,
                $package['version'],
                $package['license'] ? implode(', ', $package['license']) : null,
                null,
                'fab fa-php',
            ];
        }

        return $return;
    }

    /**
     * Reads the assets folder
     *
     * @return array
     */
    private function getAssetsVersions(): array
    {
        $return = array();

        $assets = glob(__DIR__ . '/../../public/assets/*', GLOB_ONLYDIR);
        foreach ($assets as $asset) {
            preg_match('/^([a-z\-]+)\-([0-9\.]+|master)/i', basename($asset), $matches);
            if (count($matches) < 3) {
                continue;
            }
            $package = $matches[1];
            $packageUrl = null;
            $version = $matches[2];
            $license = null;
            $licenseUrl = null;
            $icon = 'fab fa-js-square';

            switch ($package) {
                case 'bootstrap':
                    $icon = 'fab fa-bootstrap';
                    $packageUrl = 'https://getbootstrap.com/';
                    $license = 'CC-BY-3.0';
                    $licenseUrl = 'https://creativecommons.org/licenses/by/3.0/';
                    break;
                case 'jquery':
                    $icon = 'fab fa-js-square';
                    $packageUrl = 'https://jquery.com/';
                    $license = 'MIT license';
                    $licenseUrl = 'https://jquery.org/license/';
                    break;
                case 'MaterialDesign-Webfont':
                    $icon = 'fab fa-css3-alt';
                    $packageUrl = 'https://materialdesignicons.com/';
                    $license = 'MIT license';
                    $licenseUrl = 'https://raw.githubusercontent.com/Templarian/MaterialDesign/master/LICENSE';
                    break;
                case 'fontawesome-free':
                    $icon = 'fab fa-font-awesome';
                    $packageUrl = 'https://fontawesome.com/';
                    $license = 'License info';
                    $licenseUrl = 'https://fontawesome.com/license/free';
                    break;
            }

            $return[] = [$package, $packageUrl, $version, $license, $licenseUrl, $icon];
        }

        return $return;
    }

    /**
     * Since I also bundle this together with PHP Desktop in another repository,
     * show it as dependency when it exists as well
     *
     * @return string|null
     */
    private function getDesktopVersion(): ?string
    {
        if (file_exists(__DIR__ . '/../../phpdesktop-chrome.exe')) {
            return (new FsTools())->getFileVersion(__DIR__ . '/../../phpdesktop-chrome.exe');
        }
        return null;
    }
}
