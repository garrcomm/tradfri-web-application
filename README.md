# Ikea TRÅDFRI Client for PHP

**This code is currently very much in progress. It's interfaces will probably change a lot before the first release.**

This project connects to an IKEA TRÅDFRI Gateway using CoAP and provides a simple web interface to interact with your IKEA Smart Home devices.

**TOC**

* [Get your environment ready](#getting-your-environment-ready)
    * [Using Docker](#using-docker)
    * [Using PHP for Windows](#using-php-for-windows)
* [Add security to this application](#add-security-to-this-application)
* [Demo mode](#demo-mode)
* [Tested with IKEA TRÅDFRI products](#tested-with-ikea-trdfri-products)
* [External resources](#external-resources)

## Getting your environment ready

You can use this application in different ways.

* [Using Docker](#using-docker) (Windows/Linux/MacOS)  
  The easiest method is by using Docker; a pre-configured Docker-container will be loaded with all requirements available.
* [Using PHP for Windows](#using-php-for-windows) (Windows)  
  Follow these steps if you want to use this application on Windows with PHP.

### Using Docker

Docker comes with an installer for Windows and MacOS. For Linux it's a bit more complicated, but follow these links if you don't have Docker installed yet.

* Windows installer: [Docker Desktop for Windows](https://hub.docker.com/editions/community/docker-ce-desktop-windows/).
* MacOS installer: [Docker Desktop for Mac](https://hub.docker.com/editions/community/docker-ce-desktop-mac/).
* Linux: please follow the instructions to install [Docker](https://docs.docker.com/engine/install/#server) and [Docker Compose](https://docs.docker.com/compose/install/).

When Docker is installed, only a few steps are required;

1. Open a terminal/command window in the root folder of this project and type:

        docker-compose up -d

2. Use composer to fetch all PHP dependencies:

        docker-compose exec php composer install


From that moment on, the application is available at [http://localhost/](http://localhost/).

### Using PHP for Windows

To run this on Windows, you'll need three things:

* [PHP for Windows](https://windows.php.net/download/#php-7.3-ts-VC15-x64) (at least 7.3)
* The coap-client [built for Windows](https://github.com/glenndehaan/ikea-tradfri-coap-docs/blob/master/scripts/build-libcoap-win-x64.cmd)
* [composer.phar](https://getcomposer.org/composer-stable.phar)

Download those files and place them in the `win32` folder.
When you've done that, only two more steps are required;

1. Open a command window in the root folder of this project and use composer to fetch all PHP dependencies:

        bin\composer.cmd install

2. Start the PHP internal webserver

        bin\server.cmd


From that moment on, the application is available at [http://localhost/](http://localhost/).

## Add security to this application

There are two ways of security built in in the PHP framework that's used for this application.
Copy [config/security.ini.example](config/security.ini.example) to `security.ini` and modify it to your needs.

## Demo mode

When you don't have a TRÅDFRI gateway on hand and want to work on the code, it's possible to use the coap-client simulator that's included in the `php-tradfri` library.

To do so, you'll need to add this to any .ini file in the `config` folder:

```ini
[tradfri]
; Use this for Linux
coap-client = vendor/garrcomm/php-tradfri/tests/CoapClient/coap-client
; Use this for Windows
coap-client = bin/coap-client-simulator.cmd
```

To connect to a gateway, connect by IP to `127.0.0.1` and `valid-security-code` as security code.

## Tested with IKEA TRÅDFRI products

This application uses a PHP TRÅDFRI library.
See [this page](https://bitbucket.org/garrcomm/tradfri-php-library/src/master/supported-devices.md) to see which products are used during tests.

## External resources

Before I started on this project, I used Google to find something simular. I did not find much PHP related that I could use, but I found some useful resources:

* A basic PHP-wrapper for the libcoap coap-client. I didn't use this because it only has 1 commit and is not a complete application, nor a ready-to-use package, but it was a good inspiration nonetheless;  
  https://github.com/MiniMeOSc/phpTradfri
* Very well written documentation on how the TRÅDFRI gateway works with CoAP:  
  https://github.com/glenndehaan/ikea-tradfri-coap-docs
* A list of many constants that helped identifying the numeric values in the CoAP protocol:  
  https://gist.github.com/r41d/5d62033f88b3046bccf406c9158d4e59
