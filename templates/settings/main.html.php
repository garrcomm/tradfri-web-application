<?php

/* @var $baseHref string */
/* @var $tradfriGateway \Garrcomm\Tradfri\Model\TradfriGateway */
/* @var $tradfriGroupedDevices \Garrcomm\Tradfri\Model\BaseTradfriDevice[][] */
?>
<div class="row">
    <div class="col-12">
        <p>
            This app doesn't tamper with the TRÅDFRI Gateway settings.
            Instead, you can change the App settings here, and see the versions of all connected devices.
            <br>
            The release notes for the IKEA TRÅDFRI product line can be found <a href="https://ww8.ikea.com/ikeahomesmart/releasenotes/releasenotes.html" rel="nofollow" target="_blank">here <i class="fas fa-external-link-alt fa-xs"></i></a>.
        </p>

        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#disconnectModal">Disconnect app</button>
        <a class="btn btn-outline-primary" href="<?= htmlspecialchars($baseHref) ?>settings/licenses">Software licenses</a>
        <a class="btn btn-outline-primary" href="<?= htmlspecialchars($baseHref) ?>settings/debug">Debugging output</a>
        <br>
        &nbsp;
    </div>
</div>

<table class="table">
    <tr>
        <th class="td-min-width"><i class="fas fa-ethernet"></i></th>
        <th>Gateway</th>
        <th>&nbsp;</th>
    </tr>
    <tr>
        <td class="td-min-width">&nbsp;</td>
        <td>&nbsp;</td>
        <td><?= htmlspecialchars($tradfriGateway->getVersion()) ?></td>
    </tr>
    <?php foreach ($tradfriGroupedDevices as $group => $devices) : ?>
        <tr>
            <th class="td-min-width">
                <?php if ($group == 'bulb') : ?>
                    <i class="far fa-lightbulb fa-fw"></i>
                <?php elseif ($group == 'sound remote') : ?>
                    <i class="fas fa-power-off fa-fw"></i>
                <?php elseif ($group == 'blind') : ?>
                    <i class="fas fa-wifi fa-fw"></i>
                <?php elseif ($group == 'driver') : ?>
                    <i class="far fa-lightbulb fa-fw"></i>
                <?php elseif ($group == 'outlet') : ?>
                    <i class="fas fa-plug fa-fw"></i>
                <?php elseif ($group == 'signal repeater') : ?>
                    <i class="fas fa-wifi fa-fw"></i>
                <?php elseif ($group == 'remote control') : ?>
                    <i class="fas fa-power-off fa-fw"></i>
                <?php elseif ($group == 'motion sensor') : ?>
                    <i class="fas fa-wifi fa-fw"></i>
                <?php elseif ($group == 'on-off switch/dimmer') : ?>
                    <i class="fas fa-power-off fa-fw"></i>
                <?php elseif ($group == 'shortcut button') : ?>
                    <i class="fas fa-power-off fa-fw"></i>
                <?php else : ?>
                    <i class="fas fa-wifi fa-fw"></i>
                <?php endif; ?>
            </th>
            <th>
                <?= count($devices) ?> <?= htmlspecialchars(ucfirst($group . (count($devices) != 1 ? 's' : ''))) ?>
            </th>
            <th>&nbsp;</th>
        </tr>
        <?php foreach ($devices as $device) : ?>
            <tr class="<?= ($device->isReachable() ? '' : 'text-muted') ?>">
                <td class="td-min-width">&nbsp;</td>
                <td>
                    <?= htmlspecialchars($device->getName()) ?>
                    <?php if (method_exists($device, 'getBatteryLevel')) : ?>
                        <div class="float-right" title="<?= $device->getBatteryLevel() ?>%" data-toggle="tooltip" data-placement="left">
                            <?php if ($device->getBatteryLevel() > 95) : ?>
                                <i class="fas fa-battery-full"></i>
                            <?php elseif ($device->getBatteryLevel() > 70) : ?>
                                <i class="fas fa-battery-three-quarters"></i>
                            <?php elseif ($device->getBatteryLevel() > 45) : ?>
                                <i class="fas fa-battery-half"></i>
                            <?php elseif ($device->getBatteryLevel() > 20) : ?>
                                <i class="fas fa-battery-quarter"></i>
                            <?php else : ?>
                                <i class="fas fa-battery-empty"></i>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </td>
                <td class="td-min-width"><?= htmlspecialchars($device->getVersion()) ?></td>
            </tr>
        <?php endforeach; ?>
    <?php endforeach; ?>
</table>
<?php require __DIR__ . '/../connect/disconnect_modal.html.php'; ?>

