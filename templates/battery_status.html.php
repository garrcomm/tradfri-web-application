<?php

/* @var $tradfriDevices \Garrcomm\Tradfri\Model\BaseTradfriDevice[] */
/* @var $tradfriGroups \Garrcomm\Tradfri\Model\TradfriGroup[] */

// Not all pages have this data available.
// But it doesn't matter if a battety state is a bit outdated, so we'll cache those values
$batteryCacheFile = \Miniframe\Core\Registry::get('config')->getPath('tradfri', 'battery_cache_file');
if (isset($tradfriDevices) && isset($tradfriGroups)) {
    file_put_contents($batteryCacheFile, serialize([
        'tradfriDevices' => $tradfriDevices,
        'tradfriGroups' => $tradfriGroups
    ]));
} elseif (file_exists($batteryCacheFile)) {
    extract(unserialize(file_get_contents($batteryCacheFile)));
}

$batteryLevels = array();
if (isset($tradfriDevices)) {
    foreach ($tradfriDevices as $device) { /* @var $device \Garrcomm\Tradfri\Model\TradfriRemote */
        if (method_exists($device, 'getBatteryLevel')) {
            $batteryLevels[$device->getId()] = array(
                'group' => null,
                'device' => $device->getName(),
                'batteryLevel' => $device->getBatteryLevel(),
            );
        }
    }
}
if (isset($tradfriGroups)) {
    foreach ($tradfriGroups as $tradfriGroup) {
        foreach ($tradfriGroup->getDevices() as $device) { /* @var $device \Garrcomm\Tradfri\Model\TradfriRemote */
            if (method_exists($device, 'getBatteryLevel')) {
                $batteryLevels[$device->getId()] = array(
                    'group' => $tradfriGroup->getName(),
                    'device' => $device->getName(),
                    'batteryLevel' => $device->getBatteryLevel(),
                );
            }
        }
    }
}
?>
<br>
<br>
<table class="table" style="table-layout: fixed;">
    <?php foreach ($batteryLevels as $batteryLevel) : ?>
    <tr>
        <td>
            <?php
                $text = htmlspecialchars(
                    $batteryLevel['device']
                     . ($batteryLevel['group'] ? ' (' . $batteryLevel['group'] . ')' : '')
                );
            ?>
            <div style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;" title="<?= $text ?>" data-toggle="tooltip" data-placement="bottom">
                <?= $text ?>
            </div>
        </td>
        <td class="text-right td-min-width" title="<?= $batteryLevel['batteryLevel'] ?>%" data-toggle="tooltip" data-placement="right">
            <?php if ($batteryLevel['batteryLevel'] > 95) : ?>
                <i class="fas fa-battery-full"></i>
            <?php elseif ($batteryLevel['batteryLevel'] > 70) : ?>
                <i class="fas fa-battery-three-quarters"></i>
            <?php elseif ($batteryLevel['batteryLevel'] > 45) : ?>
                <i class="fas fa-battery-half"></i>
            <?php elseif ($batteryLevel['batteryLevel'] > 20) : ?>
                <i class="fas fa-battery-quarter"></i>
            <?php else : ?>
                <i class="fas fa-battery-empty"></i>
            <?php endif; ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
