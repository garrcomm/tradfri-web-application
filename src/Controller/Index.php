<?php

namespace App\Controller;

use Miniframe\Core\Response;
use Miniframe\Response\JsonResponse;
use Miniframe\Response\NotFoundResponse;
use Miniframe\Response\PhpResponse;
use Miniframe\Response\RedirectResponse;

class Index extends AbstractTradfriEnabledController
{
    /**
     * This method responds to `/`
     *
     * @return Response
     */
    public function main(): Response
    {
        return new PhpResponse(__DIR__ . '/../../templates/loading.html.php', [
            'baseHref' => $this->config->get('framework', 'base_href'),
            'page'     => 'home',
            'redirect' => 'index/home',
        ]);
    }

    /**
     * This method responds to `/index/home`
     *
     * @return Response
     */
    public function home(): Response
    {
        return new PhpResponse(__DIR__ . '/../../templates/index.html.php', [
            'baseHref'       => $this->config->get('framework', 'base_href'),
            'page'           => 'home',
            'tradfriGroups'  => $this->listGroups(),
            'tradfriDevices' => $this->listDevices(),
            'tradfriScenes'  => $this->listScenes(),
        ]);
    }

    /**
     * This method responds to `/index/on` and turns on a device
     *
     * @return Response
     */
    public function on(): Response
    {
        // Looks up the device; if not found, throw a 404 Not Found page
        $deviceId = $this->request->getPath(2);
        if (!$deviceId || !$device = $this->tradfriService->getDevice($deviceId)) {
            throw new NotFoundResponse();
        } elseif (method_exists($device, 'turnOn')) {
            $device->turnOn();
        } else {
            // Device doesn't have the turnOn capability, so this request is bad!
            $response = new Response('Bad Request', 1);
            $response->setResponseCode(400);
            throw $response;
        }

        // When the call is an Ajax call, return a boolean success
        if ($this->request->getRequest('ajax')) {
            return new JsonResponse($device);
        }

        // When it's a regular call, redirect to the homepage
        return new RedirectResponse($this->config->get('framework', 'base_href'));
    }

    /**
     * This method responds to `/index/off` and turns on a device
     *
     * @return Response
     */
    public function off(): Response
    {
        // Looks up the device; if not found, throw a 404 Not Found page
        $deviceId = $this->request->getPath(2);
        if (!$deviceId || !$device = $this->tradfriService->getDevice($deviceId)) {
            throw new NotFoundResponse();
        } elseif (method_exists($device, 'turnOff')) {
            $device->turnOff();
        } else {
            // Device doesn't have the turnOn capability, so this request is bad!
            $response = new Response('Bad Request', 1);
            $response->setResponseCode(400);
            throw $response;
        }

        // When the call is an Ajax call, return a boolean success
        if ($this->request->getRequest('ajax')) {
            return new JsonResponse($device);
        }

        // When it's a regular call, redirect to the homepage
        return new RedirectResponse($this->config->get('framework', 'base_href'));
    }

    /**
     * This method responds to `/index/brightness` and defines the brightness of a device
     *
     * @return Response
     */
    public function brightness(): Response
    {
        // Looks up the device; if not found, throw a 404 Not Found page
        $deviceId = $this->request->getPath(2);
        $brightness = (int)$this->request->getPath(3);
        if (!$deviceId || !$device = $this->tradfriService->getDevice($deviceId)) {
            throw new NotFoundResponse();
        } elseif (method_exists($device, 'setBrightness')) {
            $device->setBrightness($brightness);
        } else {
            // Device doesn't have the turnOn capability, so this request is bad!
            $response = new Response('Bad Request', 1);
            $response->setResponseCode(400);
            throw $response;
        }

        // When the call is an Ajax call, return a boolean success
        if ($this->request->getRequest('ajax')) {
            return new JsonResponse($device);
        }

        // When it's a regular call, redirect to the homepage
        return new RedirectResponse($this->config->get('framework', 'base_href'));
    }

    /**
     * This method responds to `/index/position` and defines the position of a device
     *
     * @return Response
     */
    public function position(): Response
    {
        // Looks up the device; if not found, throw a 404 Not Found page
        $deviceId = $this->request->getPath(2);
        $position = (float)$this->request->getPath(3);
        if (!$deviceId || !$device = $this->tradfriService->getDevice($deviceId)) {
            throw new NotFoundResponse();
        } elseif (method_exists($device, 'setPosition')) {
            $device->setPosition($position);
        } else {
            // Device doesn't have the setPosition capability, so this request is bad!
            $response = new Response('Bad Request', 1);
            $response->setResponseCode(400);
            throw $response;
        }

        // When the call is an Ajax call, return a boolean success
        if ($this->request->getRequest('ajax')) {
            return new JsonResponse($device);
        }

        // When it's a regular call, redirect to the homepage
        return new RedirectResponse($this->config->get('framework', 'base_href'));
    }

    /**
     * This method responds to `/index/color` and sets the color of a device
     *
     * @return Response
     */
    public function color(): Response
    {
        $deviceId = $this->request->getPath(2);
        $color = $this->request->getPath(3);
        if (!$deviceId || !$color || !$device = $this->tradfriService->getDevice($deviceId)) {
            throw new NotFoundResponse();
        } elseif (method_exists($device, 'setRgbColor')) {
            $device->setRgbColor($color);
        } else {
            // Device doesn't have the turnOn capability, so this request is bad!
            $response = new Response('Bad Request', 1);
            $response->setResponseCode(400);
            throw $response;
        }

        // When the call is an Ajax call, return a boolean success
        if ($this->request->getRequest('ajax')) {
            return new JsonResponse($device);
        }

        // When it's a regular call, redirect to the homepage
        return new RedirectResponse($this->config->get('framework', 'base_href'));
    }

    /**
     * Returns a list of all devices JSON formatted
     *
     * @return Response
     */
    public function json(): Response
    {
        return new JsonResponse($this->listDevices());
    }

    /**
     * This method responds to `/index/groupOn` and turns on a group
     *
     * @return Response
     */
    public function groupOn(): Response
    {
        // Looks up the device; if not found, throw a 404 Not Found page
        $groupId = $this->request->getPath(2);
        if (!$groupId || !$group = $this->tradfriService->getGroup($groupId)) {
            throw new NotFoundResponse();
        }
        $group->turnOn();

        // When the call is an Ajax call, return a boolean success
        if ($this->request->getRequest('ajax')) {
            return new JsonResponse($group);
        }

        // When it's a regular call, redirect to the homepage
        return new RedirectResponse($this->config->get('framework', 'base_href'));
    }

    /**
     * This method responds to `/index/groupOff` and turns off a group
     *
     * @return Response
     */
    public function groupOff(): Response
    {
        // Looks up the device; if not found, throw a 404 Not Found page
        $groupId = $this->request->getPath(2);
        if (!$groupId || !$group = $this->tradfriService->getGroup($groupId)) {
            throw new NotFoundResponse();
        }
        $group->turnOff();

        // When the call is an Ajax call, return a boolean success
        if ($this->request->getRequest('ajax')) {
            return new JsonResponse($group);
        }

        // When it's a regular call, redirect to the homepage
        return new RedirectResponse($this->config->get('framework', 'base_href'));
    }

    /**
     * This method responds to `/index/groupBrightness` and defines the brightness of a group
     *
     * @return Response
     */
    public function groupBrightness(): Response
    {
        // Looks up the device; if not found, throw a 404 Not Found page
        $groupId = $this->request->getPath(2);
        $brightness = (int)$this->request->getPath(3);
        if (!$groupId || !$group = $this->tradfriService->getGroup($groupId)) {
            throw new NotFoundResponse();
        }
        $group->setBrightness($brightness);

        // When the call is an Ajax call, return a boolean success
        if ($this->request->getRequest('ajax')) {
            return new JsonResponse($group);
        }

        // When it's a regular call, redirect to the homepage
        return new RedirectResponse($this->config->get('framework', 'base_href'));
    }
}
