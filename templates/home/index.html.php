<?php

/* @var $baseHref string */
/* @var $tradfriGroups \Garrcomm\Tradfri\Model\TradfriGroup[] */
?>
<?php require __DIR__ . '/scenes.html.php'; ?>
<table data-refresh-url="<?= htmlspecialchars($baseHref) ?>index/json" data-refresh-timeout="4000" class="table">
    <?php foreach ($tradfriGroups as $tradfriGroup) : ?>
        <?php if ($tradfriGroup->getName() == 'SuperGroup') {
            continue;
        } ?>
        <?php require __DIR__ . '/group.html.php'; ?>
    <?php endforeach; ?>
</table>
