<?php

/* @var $baseHref string */
/* @var $page string */
?>
<div class="btn-group-vertical btn-block">
    <a href="<?= htmlspecialchars($baseHref) ?>" class="btn btn-<?= $page == 'home' ? 'primary' : 'light' ?>">Home</a>
    <a href="<?= htmlspecialchars($baseHref) ?>scene" class="btn btn-<?= $page == 'scene' ? 'primary' : 'light' ?>">Scenes</a>
    <a href="<?= htmlspecialchars($baseHref) ?>settings" class="btn btn-<?= $page == 'settings' ? 'primary' : 'light' ?>">Settings</a>
</div>
