<?php

namespace App\Controller;

use App\Middleware\Tradfri;
use Miniframe\Core\AbstractController;
use Miniframe\Core\Registry;
use Miniframe\Core\Response;
use Miniframe\Response\PhpResponse;
use Miniframe\Response\RedirectResponse;

class Connect extends AbstractController
{
    /**
     * Shows the interface on which the end-user can enter the Gateway details
     *
     * @return Response
     */
    public function main(): Response
    {
        if ($this->request->getPost()) {
            return $this->handlePost();
        }

        return new PhpResponse(__DIR__ . '/../../templates/connect/connect.html.php', [
            'baseHref' => $this->config->get('framework', 'base_href'),
            'input'    => 'sn',
        ]);
    }

    /**
     * Shows the interface on which the end-user can enter the Gateway details by IP address
     *
     * @return Response
     */
    public function byip(): Response
    {
        if ($this->request->getPost()) {
            return $this->handlePost();
        }

        return new PhpResponse(__DIR__ . '/../../templates/connect/connect.html.php', [
            'baseHref' => $this->config->get('framework', 'base_href'),
            'input'    => 'ip',
        ]);
    }

    /**
     * Shows a choice window when the gateway can't be reached
     *
     * @return Response
     */
    public function failed(): Response
    {
        return new PhpResponse(__DIR__ . '/../../templates/connect/failed.html.php', [
            'baseHref' => $this->config->get('framework', 'base_href'),
        ]);
    }

    /**
     * Disconnects the app from the Gateway
     *
     * @return Response
     */
    public function disconnect(): Response
    {
        Registry::get(Tradfri::class)->disconnect();
        return new RedirectResponse(
            $this->config->get('framework', 'base_href')
            . 'connect?m=' . rawurlencode(base64_encode('Disconnected from the Gateway')),
            RedirectResponse::FOUND
        );
    }

    /**
     * When one of the forms is submitted, we'll end up here
     *
     * @return Response
     */
    private function handlePost(): Response
    {
        try {
            return Registry::get(Tradfri::class)->authenticate(
                $this->request->getPost('sn'),
                $this->request->getPost('sc'),
                $this->request->getPost('ip')
            );
        } catch (\Throwable $throwable) {
            return new RedirectResponse(
                $this->config->get('framework', 'base_href')
                . 'connect?m=' . rawurlencode(base64_encode('#' . $throwable->getCode()
                . ': ' . $throwable->getMessage())),
                RedirectResponse::FOUND
            );
        }
    }
}
