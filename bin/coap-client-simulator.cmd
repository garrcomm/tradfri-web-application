@echo off

rem Validate if PHP Code Sniffer is installed
if not exist "%~dp0..\vendor\garrcomm\php-tradfri\tests\CoapClient\coap-client" (
    echo Use "composer install" to install the PHP Tradfri package first
    goto :eof
)

rem If php.exe exists, use the Windows build, otherwise try to use Docker
if exist "%~dp0..\win32\php.exe" (
    "%~dp0..\win32\php.exe" -f "%~dp0..\vendor\garrcomm\php-tradfri\tests\CoapClient\coap-client" -- %*
) else (
    docker-compose.exe exec php php vendor/garrcomm/php-tradfri/tests/CoapClient/coap-client %*
)
