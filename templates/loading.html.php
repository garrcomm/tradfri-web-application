<?php

/* @var $baseHref string */
/* @var $redirect string */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Ikea TRÅDFRI</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?= htmlspecialchars($baseHref) ?>assets/fontawesome-free-5.15.3-web/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= htmlspecialchars($baseHref) ?>assets/bootstrap-4.4.1-dist/css/bootstrap.min.css">
    <meta http-equiv="refresh" content="0;url=<?= htmlspecialchars($baseHref . $redirect) ?>" />
    <style type="text/css">
        body {
            cursor: default;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-3">
            <div class="sticky-top">
                <?php require __DIR__ . '/nav.html.php'; ?>
                <?php require __DIR__ . '/battery_status.html.php'; ?>
            </div>
        </div>
        <div class="col-9">
            <p></p>
            <i class="fas fa-spinner fa-spin fa-3x"></i>
        </div>
    </div>
</div>
<script src="<?= htmlspecialchars($baseHref) ?>assets/jquery-3.6.0/jquery-3.6.0.min.js"></script>
<script src="<?= htmlspecialchars($baseHref) ?>assets/bootstrap-4.4.1-dist/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript">
    $(function () {
        // Generic tooltips enabled
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
</body>
</html>
