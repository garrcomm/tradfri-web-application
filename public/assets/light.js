// Toggle buttons for light clickable
$('[data-device-type=light] [data-device-toggle]').click(function(e) {
    // Execute call
    $.ajax({url: $(this).attr('href'), data: { ajax: 1 }, success: function(data) {
        refreshLight(data);
        updateGroupHtml($('[data-device-id=' + data.id + ']').data('group-id'));
    }});

    // Cancel click event
    e.preventDefault();
    return false;
});

// Color buttons for light clickable
$('[data-device-type=light] [data-device-color]').click(function(e) {
    // Execute call
    $.ajax({url: $(this).attr('href'), data: { ajax: 1 }, success: function(data) {
        refreshLight(data);
        updateGroupHtml($('[data-device-id=' + data.id + ']').data('group-id'));
    }});

    // Cancel click event
    e.preventDefault();
    return false;
});

// Brightness bars hoverable
$('[data-device-type=light] .progress-bar.clickable').parent().mousemove(function(event) {
    let positionOverElement = (event.pageX - $(this).offset().left);
    let percentage = 100 / $(this).width() * positionOverElement;
    $(this).attr('data-original-title', 'Click to set to ' + Math.ceil(percentage) + '%');
    $(this).tooltip('update').tooltip('show');
});

// Brightness bars clickable
$('[data-device-type=light] .progress-bar.clickable').parent().click(function(event) {
    let positionOverElement = (event.pageX - $(this).offset().left);
    let newValue = 254 / $(this).width() * positionOverElement;
    let href = $(this).data('base-href') + Math.ceil(newValue);

    // Execute call
    $.ajax({url: href, data: { ajax: 1 }, success: function(data) {
        refreshLight(data);
        updateGroupHtml($('[data-device-id=' + data.id + ']').data('group-id'));
    }});
});

// Show a pointer to make sure it looks clickable
$('[data-device-type=light] .progress-bar.clickable').parent().css('cursor', 'pointer');

function refreshLight(deviceData) {
    let deviceElement = $('[data-device-type=light][data-device-id=' + deviceData.id + ']');
    // Change toggle visibility
    if (deviceData.on) {
        deviceElement.find('.show-when-on').removeClass('d-none');
        deviceElement.find('.show-when-off').addClass('d-none');
    } else {
        deviceElement.find('.show-when-on').addClass('d-none');
        deviceElement.find('.show-when-off').removeClass('d-none');
    }
    // Set color
    if (typeof deviceData.rgbColor !== 'undefined') {
        deviceElement.find('.show-color').css('color', '#' + deviceData.rgbColor);
    }
    // Set brightness
    let newValuePercentage = Math.ceil(deviceData.brightness / 254 * 100);
    deviceElement.find('.progress-bar.clickable').css('width', newValuePercentage + '%');
    deviceElement.find('.progress-bar.clickable').text(newValuePercentage + '%');
}
