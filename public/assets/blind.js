// Toggle buttons for blinds clickable
$('[data-device-type=blind] a').click(function(e) {
    // Execute call
    $.ajax({url: $(this).attr('href'), data: { ajax: 1 }, success: function(data) {
        refreshBlind(data);
    }});

    // Cancel click event
    e.preventDefault();
    return false;
});

// Position bars hoverable
$('[data-device-type=blind] .progress-bar.clickable').parent().mousemove(function(event) {
    let positionOverElement = (event.pageX - $(this).offset().left);
    let percentage = 100 / $(this).width() * positionOverElement;
    $(this).attr('data-original-title', 'Click to set to ' + Math.ceil(percentage) + '%');
    $(this).tooltip('update').tooltip('show');
});

// Position bars clickable
$('[data-device-type=blind] .progress-bar.clickable').parent().click(function(event) {
    let positionOverElement = (event.pageX - $(this).offset().left);
    let newValuePercentage = Math.ceil(100 / $(this).width() * positionOverElement);
    let href = $(this).data('base-href') + Math.ceil(newValuePercentage);

    // Execute call
    $.ajax({url: href, data: { ajax: 1 }, success: function(data) {
        refreshBlind(data);
    }});
});

// Show a pointer to make sure it looks clickable
$('[data-device-type=blind] .progress-bar.clickable').parent().css('cursor', 'pointer');

function refreshBlind(deviceData) {
    let newValue = Math.ceil(deviceData.position);
    let deviceElement = $('[data-device-type=blind][data-device-id=' + deviceData.id + ']');
    deviceElement.find('.progress-bar.clickable').css('width', newValue + '%');
    deviceElement.find('.progress-bar.clickable').text(newValue + '%');
}
