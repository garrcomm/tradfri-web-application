<?php

/* @var $dependencies array */
?>
<div class="row">
    <div class="col-12">
        <p>
            This application is a hobby project made by me (Stefan Thoolen) released under the
            <a href="https://creativecommons.org/licenses/by-sa/4.0/deed" rel="nofollow" target="_blank">CC-BY-SA-4.0 <i class="fas fa-external-link-alt fa-xs"></i></a> license.
        </p>
        <p>
            <strong>I am not affiliated with IKEA</strong>, I love their smart home products, and noticed a lack of support for PHP and Windows, so I built this myself.
            But I am in no way affiliated with IKEA nor am I sponsored in any way to work on this.
        </p>
        <p>
            There is no <strong>privacy policy</strong> available, because this application itself works completely without cookies and doesn't share any data with anyone.
            There are also no <strong>terms nor conditions</strong>. I can't give any warranties on a hobby project; I programmed this mostly for myself and share it as-is.
        </p>
        <p>
            Besides my own libraries, this application also has a few other dependencies:
        </p>
    </div>
</div>

<table class="table">
    <?php foreach ($dependencies as $dependency) : ?>
    <tr>
        <td>
            <i class="<?= htmlspecialchars($dependency[5]) ?> fa-fw"></i>
            <?php if ($dependency[1]) : ?>
                <a href="<?= htmlspecialchars($dependency[1]) ?>" target="_blank" rel="nofollow"><?= htmlspecialchars($dependency[0]) ?> <i class="fas fa-external-link-alt fa-xs"></i></a>
            <?php else : ?>
                <?= htmlspecialchars($dependency[0]) ?>
            <?php endif; ?>
        </td>
        <td><?= htmlspecialchars($dependency[2]) ?></td>
        <td>
            <?php if ($dependency[4]) : ?>
                <a href="<?= htmlspecialchars($dependency[4]) ?>" target="_blank" rel="nofollow"><?= htmlspecialchars($dependency[3]) ?> <i class="fas fa-external-link-alt fa-xs"></i></a>
            <?php elseif ($dependency[3] == 'CC-BY-SA-4.0') : ?>
                <a href="https://creativecommons.org/licenses/by-sa/4.0/deed" target="_blank" rel="nofollow"><?= htmlspecialchars($dependency[3]) ?> <i class="fas fa-external-link-alt fa-xs"></i></a>
            <?php else : ?>
                <?= htmlspecialchars($dependency[3]) ?>
            <?php endif; ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
