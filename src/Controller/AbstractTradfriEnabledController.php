<?php

namespace App\Controller;

use App\Middleware\Tradfri as TradfriMiddleware;
use Garrcomm\Tradfri\Model\BaseTradfriDevice;
use Garrcomm\Tradfri\Model\TradfriBlind;
use Garrcomm\Tradfri\Model\TradfriGateway;
use Garrcomm\Tradfri\Model\TradfriGroup;
use Garrcomm\Tradfri\Model\TradfriLight;
use Garrcomm\Tradfri\Model\TradfriPlug;
use Garrcomm\Tradfri\Model\TradfriScene;
use Garrcomm\Tradfri\Service\Tradfri as TradfriService;
use Miniframe\Core\AbstractController;
use Miniframe\Core\Config;
use Miniframe\Core\Registry;
use Miniframe\Core\Request;

abstract class AbstractTradfriEnabledController extends AbstractController
{
    /**
     * Reference to the TRÅDFRI connector
     *
     * @var TradfriService
     */
    protected $tradfriService;

    /**
     * Initializes the TRÅDFRI enabled controller
     *
     * @param Request $request Reference to the Request object.
     * @param Config  $config  Reference to the Config object.
     */
    public function __construct(Request $request, Config $config)
    {
        parent::__construct($request, $config);
        $this->tradfriService = Registry::get(TradfriMiddleware::class)->getService();
    }

    /**
     * Returns a list of all known devices in the TRÅDFRI Gateway
     *
     * @return BaseTradfriDevice[]
     */
    protected function listDevices(): array
    {
        return $this->tradfriService->listDevices();
    }

    /**
     * Returns a list of all known groups in the TRÅDFRI Gateway
     *
     * @return TradfriGroup[]
     */
    protected function listGroups(): array
    {
        return $this->tradfriService->listGroups();
    }

    /**
     * Returns a list of all known scenes in the TRÅDFRI Gateway
     *
     * @return TradfriScene[]
     */
    protected function listScenes(): array
    {
        return $this->tradfriService->listScenes();
    }

    /**
     * Returns information about the Gateway device
     *
     * @return TradfriGateway
     */
    protected function getGateway(): TradfriGateway
    {
        return $this->tradfriService->getGateway();
    }

    /**
     * Groups devices
     *
     * @return BaseTradfriDevice[][]
     */
    protected function listDevicesGrouped(): array
    {
        $tradfriDevices = $this->listDevices();

        $return = array(
            'bulb'                 => [],
            'sound remote'         => [],
            'blind'                => [],
            'driver'               => [],
            'outlet'               => [],
            'signal repeater'      => [],
            'remote control'       => [],
            'motion sensor'        => [],
            'on-off switch/dimmer' => [],
            'shortcut button'      => [],
            'other'                => [],
        );

        foreach ($tradfriDevices as $tradfriDevice) {
            if (stripos($tradfriDevice->getProductName(), 'driver') !== false) {
                $return['driver'][] = $tradfriDevice;
            } elseif (stripos($tradfriDevice->getProductName(), 'repeater') !== false) {
                $return['signal repeater'][] = $tradfriDevice;
            } elseif (stripos($tradfriDevice->getProductName(), 'motion sensor') !== false) {
                $return['motion sensor'][] = $tradfriDevice;
            } elseif (stripos($tradfriDevice->getProductName(), 'sound controller') !== false) {
                $return['sound remote'][] = $tradfriDevice;
            } elseif (stripos($tradfriDevice->getProductName(), 'remote control') !== false) {
                $return['remote control'][] = $tradfriDevice;
            } elseif (stripos($tradfriDevice->getProductName(), 'shortcut button') !== false) {
                $return['shortcut button'][] = $tradfriDevice;
            } elseif (stripos($tradfriDevice->getProductName(), 'on/off switch') !== false) {
                $return['on-off switch/dimmer'][] = $tradfriDevice;
            } elseif ($tradfriDevice instanceof TradfriLight) {
                $return['bulb'][] = $tradfriDevice;
            } elseif ($tradfriDevice instanceof TradfriPlug) {
                $return['outlet'][] = $tradfriDevice;
            } elseif ($tradfriDevice instanceof TradfriBlind) {
                $return['blind'][] = $tradfriDevice;
            } else {
                $return['other'][] = $tradfriDevice;
            }
        }

        return array_filter($return);
    }
}
