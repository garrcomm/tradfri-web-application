<?php

/* @var $baseHref string */
/* @var $tradfriScenes \Garrcomm\Tradfri\Model\TradfriScene[] */
?>
<div class="row">
    <?php foreach ($tradfriScenes as $tradfriScene) : ?>
        <div class="col-sm-6">
            <a onclick="$('#activatingSceneModal').modal('show');" href="<?= htmlspecialchars($baseHref) ?>scene/activate/<?= htmlspecialchars($tradfriScene->getId()) ?>" class="btn btn-light btn-block btn-lg">
                <i class="<?= \App\Middleware\Tradfri::sceneIconIndexToClass($tradfriScene->getIconIndex()) ?> fa-lg fa-fw"></i><br>
                <?= htmlspecialchars($tradfriScene->getName()) ?>
            </a>
            <br>
        </div>
    <?php endforeach; ?>
</div>

<!-- Scene loading modal -->
<div class="modal fade" data-keyboard="false" data-backdrop="static" id="activatingSceneModal" tabindex="-1" role="dialog" aria-labelledby="activatingSceneModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="activatingSceneModalLabel">Activating scene...</h5>
            </div>
            <div class="modal-body text-center">
                <div class="spinner-border" role="status">
                    <span class="sr-only">Activating...</span>
                </div>
            </div>
        </div>
    </div>
</div>
